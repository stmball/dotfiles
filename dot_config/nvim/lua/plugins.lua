local Plug = vim.fn['plug#']

vim.call('plug#begin', '~/.vim/plugged')

	--- APPEARANCE ---
	Plug 'morhetz/gruvbox'
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'

    --- UTILS ---
    Plug 'tpope/vim-surround'
    Plug 'preservim/nerdtree'
    Plug 'windwp/nvim-autopairs'
    Plug 'justinmk/vim-sneak'

    --- LSP + AUTOCOMPLETE ---
    Plug 'williamboman/mason.nvim'
    Plug 'williamboman/mason-lspconfig.nvim'
    Plug 'neovim/nvim-lspconfig'
    Plug 'hrsh7th/nvim-cmp'
    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'hrsh7th/vim-vsnip'
    Plug 'hrsh7th/vim-vsnip-integ'


vim.call('plug#end')

--- gruvbox & airline ---
vim.g.gruvbox_transparent_bg = 1
vim.g.gruvbix_italic = 1

vim.cmd('colorscheme gruvbox')

vim.g.airline_theme = 'gruvbox'
vim.cmd('hi Normal guibg=None ctermbg=None')
vim.g.airline_powerline_fonts = 1 
vim.g.airline_skip_empty_sections = 1 

--- nerdtree ---
vim.g.NERDTreeShowHidden = 1
vim.g.NERDTreeMinimalUI = 1
--- COQ ---
vim.g.coq_settings = {
    auto_start = true
}

require('nvim-autopairs').setup{}
