# dotfiles

Dotfiles Managed using the `chezmoi` framework.

## Quick Start

* Using the [Alacritty](https://github.com/alacritty/alacritty) terminal.

* Install `chezmoi`: 

```bash
sh -c "$(curl -fsLS get.chezmoi.io)"
chezmoi init https://gitlab.com/stmball/dotfiles.git
```

* Install `oh-my-zsh`

```bash
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

* Install `tmux` and `tpm`

```bash
apt install tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

* Install `nvim`

```bash
cd ~/.local/bin
curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage
chmod u+x nvim.appimage
mv nvim.appimage nvim
```

## `chezmoi`

`chezmoi` is a `git` wrapper to manage dotfiles on a new computer - it's used for managing dotfiles using `git`.

* On installing `chezmoi`, running `chezmoi apply` will pull from the remote repo and apply the changes to the current machine.
* To add new or edit files, use the `chezmoi add` and `chezmoi edit` commands respectively. To apply these changes to the `chezmoi` folder use `chezmoi apply`.
* `chezmoi cd` will take you to the `chezmoi` folder where you can `git add . & git commit & git push` to update the local repo.

## `oh-my-zsh`

`oh-my-zsh` is a `zsh` manager that allows us to get set up quickly with `zsh`.

